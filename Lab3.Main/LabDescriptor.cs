﻿using System;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(IZarzadzanie);
        public static Type I2 = typeof(IZasilanie);
        public static Type I3 = typeof(ISterowanie);

        public static Type Component = typeof(WindaKosmiczna);

        public static GetInstance GetInstanceOfI1 = new GetInstance(WindaKosmiczna.GetInstance);
        public static GetInstance GetInstanceOfI2 = new GetInstance(WindaKosmiczna.GetInstance);
        public static GetInstance GetInstanceOfI3 = new GetInstance(WindaKosmiczna.GetInstance);
        
        #endregion

        #region P2

        public static Type Mixin = typeof(Program);
        public static Type MixinFor = typeof(ISterowanie);

        #endregion
    }
}
