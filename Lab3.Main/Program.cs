﻿using System;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    public static class Program
    {
        public static string ZatrzymajWinde(this ISterowanie obiekt)
        {
            return "Winda Zatrzymana";
        }

        static void Main(string[] args)
        {
            WindaKosmiczna winda = new WindaKosmiczna();
            Console.WriteLine(winda.ZatrzymajWinde());
            Console.WriteLine(((IZarzadzanie)winda).ZaladujPojazd());
            Console.WriteLine(((IZarzadzanie)winda).RozladujPojazd());
            Console.WriteLine(((IZasilanie)winda).WlaczZasilanie());
            Console.WriteLine(((IZasilanie)winda).WylaczZasilanie());
            Console.WriteLine(((ISterowanie)winda).JedzWGore());
            Console.WriteLine(((ISterowanie)winda).JedzWDol());         
            Console.ReadKey();

        }
    }
}
