﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3.Implementation
{
    public class WindaKosmiczna : IZarzadzanie, IZasilanie, ISterowanie
    {
        private PanelKontrolny pk = new PanelKontrolny();
        private Platforma plat = new Platforma();
        private Zasilanie zas = new Zasilanie();

        string IZarzadzanie.RozladujPojazd()
        {
            return plat.RozladujPojazd();
        }

        string IZarzadzanie.ZaladujPojazd()
        {
            return plat.ZaladujPojazd();
        }



        string IZasilanie.WlaczZasilanie()
        {
            return zas.WlaczZasilanie();
        }

        string IZasilanie.WylaczZasilanie()
        {
            return zas.WylaczZasilanie();
        }

        string ISterowanie.JedzWGore()
        {
            return pk.JedzWGore();
        }

        string ISterowanie.JedzWDol()
        {
            return pk.JedzWDol();
        }

        public static object GetInstance(object component)
        {
            return new WindaKosmiczna() ;
        }
    }
}
